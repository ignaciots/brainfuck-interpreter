#ifndef GUARD_BCC_H
#define GUARD_BCC_H

/*
 * bcc functions and constant declarations
 */

#define PROGRAM_NAME  "bcc"
#define PROGRAM_USAGE "Usage: %s <branfuck_program>\n"

/**
 * Prints the program usage
 **/
void print_usage();

/**
 * Executes the program with a given input
 * input: The given input
 * Return: EXIT_SUCCESS if successful, EXIT_FAILURE otherwise.
 **/
int bcc(char *program, char *input);
#endif /* ifndef GUARD_BCC_H */
