#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "inst/brainfuck_instructions.h"
#include "parser/brainfuck_parser.h"
#include "exec/brainfuck.h"

#include "bcc.h"

static unsigned int steps      = 0; // number of steps used to execute the program
static unsigned int characters = 0; // number of characters of the program

/**
 * Prints a compiler line error.
 * err_msg: The erro message.
 * err_line: The line number of the error.
 * err_col: The column number of the error.
 **/
static void print_line_err(const char * err_msg, const char * input, unsigned int err_line, unsigned int err_col);

/**
 * Prints the number of steps and characters used to execute the program.
 **/
static void print_program_stats();

/**
 * Handler for sigint that prints the program's stats.
 * signum: The interruption number identifier.
 **/
static void sigint_print_stats_handler(int signum);


int main(int argc, char * argv[])
{
    if (argc < 3) {
        fprintf(stderr, "Not enough arguments\n");
        print_usage();
        return EXIT_FAILURE;
    } else if (argc > 3) {
        fprintf(stderr, "Too many arguments\n");
        print_usage();
        return EXIT_FAILURE;
    } else {
        return bcc(argv[1], argv[2]);
    }
}

int bcc(char *program, char *input)
{
    unsigned int instructions_size, err_line, err_col;
    int status_result;
    enum brainfuck_instructions * instructions;

    characters = strlen(program);

    status_result = parse_instructions(program, &instructions, &instructions_size, &err_line, &err_col);
    switch (status_result) {
        case BRAINFUCK_PARSER_VALID:
            break;
        case BRAINFUCK_PARSER_ERROR_WHILE_RIGHTFIRST:
            print_line_err("Error: unopened right bracket.\n", input, err_line, err_col);
            return EXIT_FAILURE;

        case BRAINFUCK_PARSER_ERROR_WHILE_UNBALANCED:
            print_line_err("Error: unbalanced brackets.\n", input, err_line, err_col);
            return EXIT_FAILURE;

        case BRAINFUCK_PARSER_ERROR_UNKNOWNCHAR:
            print_line_err("Error: unknown character.\n", input, err_line, err_col);
            return EXIT_FAILURE;

        default:
            fprintf(stderr, "Error: unknown status code.\n");
            return EXIT_FAILURE;
    }

    struct sigaction int_action;
    int_action.sa_handler = sigint_print_stats_handler;
    if (sigaction(SIGINT, &int_action, NULL) != 0) {
        fprintf(stderr, "Error: cannot create signal handler for SIGINT\n");
        free(instructions);
        return EXIT_FAILURE;
    }

    status_result = brainfuck_execute(instructions, instructions_size, &steps, input, strlen(input));
    free(instructions);

    switch (status_result) {
        case BRAINFUCK_VALID:
            print_program_stats();
            return EXIT_SUCCESS;

        case BRAINFUCK_ERROR_POINTER_OVERFLOW:
            fprintf(stderr, "Error: bcc pointer overflow.\n");
            return EXIT_FAILURE;

        case BRAINFUCK_ERROR_POINTER_UNDERFLOW:
            fprintf(stderr, "Error: bcc pointer underflow.\n");
            return EXIT_FAILURE;

        case BRAINFUCK_ERROR:
            fprintf(stderr, "Error: bcc general error.\n");
            return EXIT_FAILURE;

        default:
            fprintf(stderr, "Error: bcc unknown error.\n");
            return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
} /* bcc */

void print_usage()
{
    printf(PROGRAM_USAGE, PROGRAM_NAME);
}

static void print_line_err(const char * err_msg, const char * input, unsigned int err_line, unsigned int err_col)
{
    unsigned int input_size   = strlen(input);
    char * error_pointer_line = calloc(input_size + 1, 1);
    unsigned int i;

    for (i = 1; i <= input_size; i++) {
        if (i == err_col) {
            *(error_pointer_line + i - 1) = '^';
        } else {
            *(error_pointer_line + i - 1) = ' ';
        }
    }

    fprintf(stderr, "Compilation error at line %d, column %d: %s", err_line, err_col, err_msg);
    fprintf(stderr, "%s\n%s\n", input, error_pointer_line);

    free(error_pointer_line);
}

static void print_program_stats()
{
    printf("\n\nProgram completed with %d steps and %d characters.\n", steps, characters);
}

static void sigint_print_stats_handler(int signum)
{
    print_program_stats();
    exit(EXIT_SUCCESS);
}
