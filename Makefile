
# rgb asm, linker and fixer
TARGET = bcc
COMPILER = gcc

.PHONY: clean all default

default: $(TARGET)
all: default

SOURCES = bcc.c parser/brainfuck_parser.c exec/brainfuck.c
OBJECTS = bcc.o brainfuck_parser.o brainfuck.o

$(OBJECTS): $(SOURCES)
	$(COMPILER) -I . -c $(SOURCES)

.PRECIOUS: $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(COMPILER) -Wall -Wextra -Wshadow -o $(TARGET) $(OBJECTS)

clean:
	rm -f $(OBJECTS) $(TARGET)
