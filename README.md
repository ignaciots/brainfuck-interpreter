# brainfuck
brainfuck language interpreter.
See https://en.wikipedia.org/wiki/Brainfuck

## How to build

To build the program, execute the following command.
```
make
```

## Program usage

To use the program, execute the following command:
```
./bcc "program"
```
where *program* is the brainfuck program you want tu run.

For example, executing
```
 ./bcc "+[.+]"
```
produces the following output:
```



123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~

Program completed with 767 steps and 5 characters.
```
