#include <stdio.h>
#include <stdlib.h>

#include "inst/brainfuck_instructions.h"
#include "brainfuck.h"

static unsigned char * brainfuck_memory;
static unsigned char * brainfuck_pointer; //pointer to the current memory cell
static unsigned char * brainfuck_input;
static enum brainfuck_instructions * instruction_array;

static unsigned int brainfuck_size; // brainfuck instructions size
static unsigned int brainfuck_input_size;
static unsigned int instruction_pointer; // program counter
static unsigned int input_pointer; // index of the next input data that will be read from brainfuck_input

/**
 * Increments the data pointer (to point to the next cell to the right).
 * Return: See brainfuck return status code.
 **/
static int inc_pointer();

/**
 * Decrements the data pointer (to point to the next cell to the left).
 **/
static int dec_pointer();

/**
 * Increments (increase by one) the byte at the data pointer.
 **/
static void inc();

/**
 * Decrements (decrease by one) the byte at the data pointer.
 **/
static void dec();

/**
 * Outputs the byte at the data pointer.
 **/
static void output();

/**
 * Accept one byte of input, storing its value in the byte at the data pointer.
 */
static void input();

/**
 * If the byte at the data pointer is zero,
 * then instead of moving the instruction pointer forward to the next command,
 * jump it forward to the command after the matching ] command.
 **/
static void while_start();


/**
 * If the byte at the data pointer is nonzero,
 * then instead of moving the instruction pointer forward to the next command,
 * jump it back to the command after the matching [ command.
 **/
static void while_end();

/**
 * Sets the instruction pointer to the matching left bracket
 **/
static void jump_before_start();

/**
 * Sets the instruction pointer to the matching right bracket
 **/
static void jump_after_end();

int brainfuck_execute(enum brainfuck_instructions * instructions, unsigned int brainfuck_instructions_size, unsigned int * steps, unsigned char * input_data, unsigned int input_size) {
    brainfuck_memory = calloc(BRAINFUCK_MEMORY_SIZE, 1);
    brainfuck_pointer = brainfuck_memory;
    brainfuck_input = input_data;
    instruction_array = instructions;
    brainfuck_size = brainfuck_instructions_size;
    brainfuck_input_size = input_size;

    int status_code = BRAINFUCK_VALID;
    *steps = 0;
    for (instruction_pointer = 0; instruction_pointer < brainfuck_instructions_size; instruction_pointer++) {
        *steps = *steps + 1;
        enum brainfuck_instructions instruction = *(instructions + instruction_pointer); // instruction to be executed
        switch(instruction) {
            case INC_PTR:
                status_code = inc_pointer();
                break;
            case DEC_PTR:
                status_code = dec_pointer();
                break;
            case INC:
                inc();
                break;
            case DEC:
                dec();
                break;
            case OUT:
                output();
                break;
            case IN:
                input();
                break;
            case WHILE_IN:
                while_start();
                break;
            case WHILE_OUT:
                while_end();
                break;
            default:
                status_code = BRAINFUCK_ERROR;
        }
        if (status_code != BRAINFUCK_VALID) {
            free(brainfuck_memory);
            return status_code;
        }
    }

    free(brainfuck_memory);
    
    return BRAINFUCK_VALID;
}

static int inc_pointer() {
    if (brainfuck_pointer - brainfuck_memory + 1 >= BRAINFUCK_MEMORY_SIZE) {
        return BRAINFUCK_ERROR_POINTER_OVERFLOW;
    }
    brainfuck_pointer++;
    return BRAINFUCK_VALID;
}

static int dec_pointer() {
    if (brainfuck_pointer == brainfuck_memory) {
        return BRAINFUCK_ERROR_POINTER_UNDERFLOW;
    }
    brainfuck_pointer--;
    return BRAINFUCK_VALID;
}

static void inc() {
    (*brainfuck_pointer)++;
}

static void dec() {
    (*brainfuck_pointer)--;
}

static void output() {
    printf("%c", *brainfuck_pointer);
}

static void input() {
    if (input_pointer >= brainfuck_input_size) {
        *brainfuck_pointer = 0;
    }

    *brainfuck_pointer = brainfuck_input[input_pointer++];
}

static void while_start() {
    if (*brainfuck_pointer == 0) {
        jump_after_end();
    }
}

static void while_end() {
    if (*brainfuck_pointer != 0) {
        jump_before_start();
    }
}

static void jump_after_end() {
    while (*(instruction_array + instruction_pointer) != WHILE_OUT) {
        instruction_pointer++;
    }
}

static void jump_before_start() {
    while (*(instruction_array + instruction_pointer) != WHILE_IN) {
        instruction_pointer--;
    }
}
