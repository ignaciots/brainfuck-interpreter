#ifndef GUARD_BRAINFUCK_H
#define GUARD_BRAINFUCK_H

/*
 * Brainfuck data and function declarations
 */

#define BRAINFUCK_MEMORY_SIZE 30000

// Brainfuck return status codes
#define BRAINFUCK_VALID                   0
#define BRAINFUCK_ERROR_POINTER_OVERFLOW  2
#define BRAINFUCK_ERROR_POINTER_UNDERFLOW 3
#define BRAINFUCK_ERROR                   4

/**
 * Starts the brainfuck interpreter.
 * instructions: an array of instructions used in the interpreter.
 * instructions_size: the size of the array of instructions.
 * steps: Pointer  to store the number of steps used to complete the program
 * Return: See brainfuck return status code.
 **/
int brainfuck_execute(enum brainfuck_instructions *, unsigned int brainfuck_instructions_size, unsigned int * steps, unsigned char * input, unsigned int input_len);


#endif /* ifndef GUARD_BRAINFUCK_H */
