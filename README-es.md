# brainfuck
Intérprete del lenguaje brainfuck.
Ver https://en.wikipedia.org/wiki/Brainfuck

## Compilar

Para compilar el programa, ejecutar:
```
make
```

## Uso

Para usar el programa, ejecutar:
```
./bcc "program"
```
donde *programa* es el programa brainfuck que quieres ejecutar.

Por ejemplo, ejecutar
```
 ./bcc "+[.+]"
```
produce la siguiente salida.
```



123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~

Program completed with 767 steps and 5 characters.
```
