#ifndef GUARD_BRAINFUCK_PARSER_H
#define GUARD_BRAINFUCK_PARSER_H

/*
 * Brainfuck parser functions and constants
 */

// Brainfuck parser status codes
#define BRAINFUCK_PARSER_VALID                  0
#define BRAINFUCK_PARSER_ERROR_WHILE_RIGHTFIRST 1
#define BRAINFUCK_PARSER_ERROR_WHILE_UNBALANCED 2
#define BRAINFUCK_PARSER_ERROR_UNKNOWNCHAR      3

/**
 * Parses brainfuck instructions and creates an abstract instruction array
 * instructions_input: The input brainfuck program to be parsed as a string.
 * instruction_array: The pointer where the created abstract array pointer will be stored.
 * instruction_array_size: The size of the abstract array.
 * line_number: The place to store the line number if an error happens.
 * Return: see brainfuck parser status codes.
 **/
int parse_instructions(const char * instructions_input, enum brainfuck_instructions ** instruction_array_pointer,
  unsigned int * instruction_array_size, unsigned int * err_line_number,
  unsigned int * err_colum_number);

#endif /* ifndef GUARD_BRAINFUCK_PARSER_H */
