#include <stdlib.h>
#include <string.h>

#include "inst/brainfuck_instructions.h"
#include "brainfuck_parser.h"

static const char STRIP_CHARACTERS[] = { '\t', ' ' };
static const unsigned int STRIP_CHARACTERS_LEN = 2;

/**
 * Strips program whitespaces and tabulations
 * instructions_input: The input brainfuck as a string.
 * strip_result_size: The size of the stripped string.
 * Return: a pointer to the new string with whitespaces stripped.
 **/
static char * strip_whitespaces(const char * instructions_input, unsigned int * strip_result_size);


int parse_instructions(const char * instructions_input, enum brainfuck_instructions ** instruction_array_pointer,
  unsigned int * instruction_array_size, unsigned int * err_line_number,
  unsigned int * err_column_number)
{
    unsigned int stripped_size;
    char * stripped_input = strip_whitespaces(instructions_input, &stripped_size);
    enum brainfuck_instructions * instruction_array_buffer = calloc(stripped_size, sizeof(enum brainfuck_instructions));

    unsigned int while_in_count          = 0;
    unsigned int while_out_count         = 0;
    unsigned int instruction_array_count = 0;
    unsigned int line_number   = 1;
    unsigned int column_number = 0;
    unsigned int i;

    for (i = 0; i < stripped_size; i++) {
        char instruction = *(stripped_input + i);
        if (instruction == '\n') {
            line_number++;
            column_number = 0;
            continue;
        } else {
            column_number++;
            instruction_array_count++;
        }

        int status_code = BRAINFUCK_PARSER_VALID;
        switch (instruction) { // Substract 1 due to size increment before
            case '>':
                *(instruction_array_buffer + instruction_array_count - 1) = INC_PTR;
                break;
            case '<':
                *(instruction_array_buffer + instruction_array_count - 1) = DEC_PTR;
                break;
            case '+':
                *(instruction_array_buffer + instruction_array_count - 1) = INC;
                break;
            case '-':
                *(instruction_array_buffer + instruction_array_count - 1) = DEC;
                break;
            case '.':
                *(instruction_array_buffer + instruction_array_count - 1) = OUT;
                break;
            case ',':
                *(instruction_array_buffer + instruction_array_count - 1) = IN;
                break;
            case '[':
                *(instruction_array_buffer + instruction_array_count - 1) = WHILE_IN;
                while_in_count++;
                *err_line_number   = line_number; // In the case of brackets being unbalanced, point to last left bracket
                *err_column_number = column_number;
                break;
            case ']':
                *(instruction_array_buffer + instruction_array_count - 1) = WHILE_OUT;
                while_out_count++;
                if (while_out_count > while_in_count) { // If number of ] is greater than [ at any point it is invalid
                    *err_line_number   = line_number;
                    *err_column_number = column_number;
                    status_code        = BRAINFUCK_PARSER_ERROR_WHILE_RIGHTFIRST;
                }
                break;
            default:
                *err_line_number   = line_number;
                *err_column_number = column_number;
                status_code        = BRAINFUCK_PARSER_ERROR_UNKNOWNCHAR;
        }
        if (status_code != BRAINFUCK_PARSER_VALID) {
            free(instruction_array_buffer);
            free(stripped_input);
            return status_code;
        }
    }

    if (while_in_count != while_out_count) { // Check if brackets are balanced
        free(instruction_array_buffer);
        free(stripped_input);
        return BRAINFUCK_PARSER_ERROR_WHILE_UNBALANCED;
    }

    enum brainfuck_instructions * instruction_array = calloc(instruction_array_count,
        sizeof(enum brainfuck_instructions));
    memcpy(instruction_array, instruction_array_buffer, instruction_array_count * sizeof(enum brainfuck_instructions));

    free(instruction_array_buffer);
    free(stripped_input);

    *instruction_array_pointer = instruction_array;
    *instruction_array_size    = instruction_array_count;
    return BRAINFUCK_PARSER_VALID;
} /* parse_instructions */

char * strip_whitespaces(const char * instructions_input, unsigned int * strip_result_size)
{
    unsigned int input_size   = strlen(instructions_input);
    char * strip_input_buffer = calloc(input_size + 1, 1); // Allocate space for null-termination

    int strip_size = 0;
    unsigned int i;

    for (i = 0; i < input_size; i++) {
        char current_input_char = *(instructions_input + i);

        unsigned int j;
        unsigned int is_strip = 0;
        for (j = 0; j < STRIP_CHARACTERS_LEN; j++) { // check if character is in strip characters list
            char current_strip_char = STRIP_CHARACTERS[j];
            if (current_strip_char == current_input_char) {
                is_strip = 1;
                break;
            }
        }
        if (!is_strip) {
            *(strip_input_buffer + strip_size) = current_input_char;
            strip_size++;
        }
    }

    char * stripped_input = calloc(strip_size + 1, 1); // Allocate space for stripped input based on its size
    strncpy(stripped_input, strip_input_buffer, strip_size);
    *strip_result_size = strip_size;
    free(strip_input_buffer);

    return stripped_input;
}
