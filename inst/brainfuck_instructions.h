#ifndef GUARD_BRAINFUCK_INSTRUCTIONS_H
#define GUARD_BRAINFUCK_INSTRUCTIONS_H

/*
 * Enum defining the brainfuck instruction set
 */
enum brainfuck_instructions { INC_PTR, DEC_PTR, INC, DEC, OUT, IN, WHILE_IN, WHILE_OUT };

#endif
